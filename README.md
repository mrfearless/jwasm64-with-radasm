# JWasm64 programming language addin for RadASM

fearless 2015 - [www.LetTheLight.in](http://www.LetTheLight.in)

## Overview

This is a collection of files to add support for JWasm/JWasm64 to the RadASM IDE
Supports 32bit JWasm and 64bit JWasm (JWasm64)

## Whats included in this package
* Jwasm.ini - main RadASM programming language file for JWasm
* DialogAsMain_x86.tpl - RadASM template for JWasm Dialog As Main Window x86
* DialogAsMain_x64.tpl - RadASM template for JWasm64 Dialog As Main Window x64
* Dll64Project.tpl - RadASM template for JWasm64 64Bit DLL Project
* x64dbg_plugin.tpl - RadASM template for JWasm64 x64dbg plugin projects
* Support folders for JWasm for RadASM: AddIns, Template, Projects etc

##Installation
* Copy JWasm.ini to \RadASM\ folder
* Copy JWasm folder to \RadASM\ folder
* Copy AddIns folder to \RadASM\ folder
* Edit radasm.ini to add jwasm to the list of Assemblers
```
    [Assembler]
    Assembler=masm,JWasm,GoAsm,fasm,nasm,html
```

## Other downloads
 * [RadASM IDE v2.2.1.6](http://www.oby.ro/rad_asm/)
 * [RadASM IDE v2.2.2.0](http://www.assembly.com.br/download/radasm.html)
 * [JWasm/JWasm64](http://masm32.com/board/index.php?topic=3795.0)
 * [x64dbg plugin SDK for JWasm64](https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-jwasm64)
 * [x64dbg plugin SDK for Masm](https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-masm)

