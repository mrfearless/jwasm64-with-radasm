@echo off
echo JWasm64 with RadASM
echo.
echo Creating download zip archive...
del .\downloads\jwasm64-with-radasm.zip > nul
"Z:\Program Files\Compression Programs\WinRAR\WinRar.exe" a -m5 -r .\downloads\jwasm64-with-radasm.zip jwasm64-with-radasm-readme.txt JWasm.ini .\JWasm\*.* .\AddIns\*.* 
echo.
echo.Finished